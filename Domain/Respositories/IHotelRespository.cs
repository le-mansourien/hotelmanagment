﻿using HotelManagment.Domain.Models;

namespace HotelManagment.Domain.Respositories
{
    public interface IHotelRespository
    {
        Task<IEnumerable<Hotel>> ListAsync();
        Task<IEnumerable<Hotel>> GetAllHotelsForGivenCityAsync(string city);
        List<Hotel> ListAll();
       
      
    }
}
