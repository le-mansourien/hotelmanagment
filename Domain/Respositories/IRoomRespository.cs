﻿using HotelManagment.Domain.Models;

namespace HotelManagment.Domain.Respositories
{
    public interface IRoomRespository
    {

        List<Room> ListAll();
        Task<IEnumerable<Room>> ListAllRoomsByHotelCodeAsync(string hotelCode);
    }
}
