﻿using HotelManagment.Domain.Models;

namespace HotelManagment.Domain.Services
{
    public interface IHotelService
    {
        Task<IEnumerable<Hotel>> ListAsync();
        string GetTheShippestHotelNameForGivenRoomType(string roomType);
        Task<IEnumerable<Hotel>> GetAllHotelsForGivenCityAsync(string city);
    }
}
