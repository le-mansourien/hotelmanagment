﻿using HotelManagment.Domain.Models;

namespace HotelManagment.Domain.Services
{
    public interface IRoomService
    {
        Task<IEnumerable<Room>> ListAllRoomsByHotelCodeAsync(string hotelCode);
    }
}
