﻿namespace HotelManagment.Domain.Models
{
    public class Hotel
    {
        
        public required string Code { get; set; }
        public string HotelName { get; set; }
        public double LocalCategory { get; set; }
        public required string City { get; set; }
        public int NumberOfRooms { get; set; }
        public List<Room> Rooms { get; set; } = new List<Room>();
        
    }
}
