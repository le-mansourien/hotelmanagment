﻿namespace HotelManagment.Domain.Models
{
    public class Room
    {
        public int Id { get; set; }
        public required string RoomName { get; set; }
        public double PricePerNight { get; set; }
        public required string HotelCode { get; set; }
        public Hotel Hotel { get; set; }

    }
    
}
