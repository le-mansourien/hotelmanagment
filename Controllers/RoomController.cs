﻿using AutoMapper;
using HotelManagment.Domain.Models;
using HotelManagment.Domain.Services;
using HotelManagment.Resources;
using Microsoft.AspNetCore.Mvc;


namespace HotelManagment.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoomController : ControllerBase
    {
        private readonly IRoomService _roomService;
        private readonly IMapper _mapper;
        public RoomController(IRoomService roomService, IMapper mapper)
        {
            _roomService = roomService;
            _mapper = mapper;
        }

        /// <summary>
        ///Alle Zimmer inkl. Preis für einen bestimmten Hotelcode
        /// </summary>
       
        
        [Route("hotel/{hotelCode}")]
        [HttpGet]
        public async Task<IActionResult> GetAllRoomsByHotelCodeAsync(string hotelCode)
        {
            ApiResponse<IEnumerable<RoomResource>> response = new ApiResponse<IEnumerable<RoomResource>>();
            try
            {
                var rooms = await _roomService.ListAllRoomsByHotelCodeAsync(hotelCode);
                var resources = _mapper.Map<IEnumerable<Room>, IEnumerable<RoomResource>>(rooms);
                response.Data = resources;

            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorMessage = ex.Message;
                return BadRequest(response);

            }
            return Ok(response);
                        
        }
      
       
    }
}
