﻿using AutoMapper;
using HotelManagment.Domain.Models;
using HotelManagment.Domain.Services;
using HotelManagment.Resources;
using Microsoft.AspNetCore.Mvc;


namespace HotelManagment.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HotelController : ControllerBase
    {
       
        private readonly IHotelService _hotelService;
        private readonly IMapper _mapper;

        public HotelController(IHotelService hotelService, IMapper mapper)
        {
            _hotelService = hotelService;
            _mapper = mapper;
        }


        /// <summary>
        ///Alle Hotels, diese Anfrage ist in die Aufgabe nicht angefragt, ist einfach um sicher zu gehen dass alle Daten
        ///steht
        /// </summary>
        [HttpGet]
        public async Task<IActionResult> GetAllAsync()
        {
            ApiResponse<IEnumerable<HotelResource>> response = new ApiResponse<IEnumerable<HotelResource>>();
            try
            {
                var hotels = await _hotelService.ListAsync();
                var resources = _mapper.Map<IEnumerable<Hotel>, IEnumerable<HotelResource>>(hotels);
                response.Data = resources;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorMessage = ex.Message;
                return BadRequest(response);

            }
            return Ok(response);
        }
        /// <summary>
        ///Alle Hotels einer Stadt, sortiert absteigend nach Lokaler Kategorie (LocalCategorie)
        /// </summary>
        /// 
        [Route("/city/{city}")]
        [HttpGet]
        public async Task<IActionResult> GetAllHotelsForGivenCityAsync(string city)
        {

            ApiResponse<IEnumerable<HotelResource>> response = new ApiResponse<IEnumerable<HotelResource>>();
            try
            {
                var hotels = await _hotelService.GetAllHotelsForGivenCityAsync(city);
                var resources = _mapper.Map<IEnumerable<Hotel>, IEnumerable<HotelResource>>(hotels);
                response.Data = resources;

            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorMessage = ex.Message;
                return BadRequest(response);

            }
            return Ok(response);

        }

        /// <summary>
        ///Das günstigste Hotel(Hotelnamen) für einen gesuchten Zimmertyp(z.B. "Double Room Standard")
        /// </summary>
        
        [Route("/roomType/{roomType}")]
        [HttpGet]
        public IActionResult GetTheShippestHotelNameForGivenRoomType(string roomType)
        {
            ApiResponse<string> response = new ApiResponse<string>();
            try
            {                
                response.Data= _hotelService.GetTheShippestHotelNameForGivenRoomType(roomType);                 
            }
            catch (Exception ex) 
            {
                response.Success = false;
                response.ErrorMessage = ex.Message;
                return BadRequest(response);

            }
            return Ok(response);
            
        }

    }
}
