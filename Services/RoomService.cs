﻿using HotelManagment.Domain.Models;
using HotelManagment.Domain.Respositories;
using HotelManagment.Domain.Services;
using HotelManagment.Persistence.Repositories;

namespace HotelManagment.Services
{
    public class RoomService : IRoomService
    {
        private readonly IRoomRespository _roomRepository;


        public RoomService(IRoomRespository roomRepository)
        {
            _roomRepository = roomRepository;

        }
       
        public async Task<IEnumerable<Room>> ListAllRoomsByHotelCodeAsync(string hotelCode)
        {
            return await _roomRepository.ListAllRoomsByHotelCodeAsync(hotelCode);
        }
    }
}
