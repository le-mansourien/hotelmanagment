﻿using HotelManagment.Domain.Models;
using HotelManagment.Domain.Respositories;
using HotelManagment.Domain.Services;

namespace HotelManagment.Services
{
    public class HotelService : IHotelService
    {
        private readonly IHotelRespository _hotelRepository;
        private readonly IRoomRespository _roomRepository;


        public HotelService(IHotelRespository hotelRepository, IRoomRespository roomRespository)
        {
            _hotelRepository = hotelRepository;
            _roomRepository =  roomRespository;
        }

        public async Task<IEnumerable<Hotel>> GetAllHotelsForGivenCityAsync(string city)
        {
            return await _hotelRepository.GetAllHotelsForGivenCityAsync(city);
        }

        public string GetTheShippestHotelNameForGivenRoomType(string roomType)
        {
            List<Room> allRoomsForTheGivenRoomType = _roomRepository.ListAll().Where(r=>r.RoomName==roomType).ToList();
            double lowest_hotel_price = allRoomsForTheGivenRoomType.Min(r => r.PricePerNight);
            string hotelName = allRoomsForTheGivenRoomType.FirstOrDefault(r => r.PricePerNight == lowest_hotel_price).Hotel.HotelName;
            return hotelName;
        }

        public async Task<IEnumerable<Hotel>> ListAsync()
        {
            return await _hotelRepository.ListAsync();
        }
      
    }
}
