﻿using AutoMapper;
using HotelManagment.Domain.Models;
using HotelManagment.Resources;

namespace HotelManagment.Mapping
{
    public class ModelToResourceProfile: Profile
    {
        public ModelToResourceProfile()
        {
            CreateMap<Room, RoomResource>();
            CreateMap<Hotel, HotelResource>();
        }
    }
}
