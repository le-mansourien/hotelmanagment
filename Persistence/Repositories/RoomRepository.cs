﻿using HotelManagment.Domain.Models;
using HotelManagment.Domain.Respositories;
using HotelManagment.Persistence.Contexts;
using Microsoft.EntityFrameworkCore;



namespace HotelManagment.Persistence.Repositories
{
    public class RoomRepository : BaseRepository, IRoomRespository
    {
        public RoomRepository(AppDbContext context) : base(context)
        {
        }

        public List<Room> ListAll()
        {
            return _context.Rooms.Include(r=>r.Hotel).ToList();
        }
        public async Task<IEnumerable<Room>> ListAllRoomsByHotelCodeAsync(string hotelCode)
        {
            return await _context.Rooms.Where(r=>r.HotelCode==hotelCode).ToListAsync();
        }
    }
}
