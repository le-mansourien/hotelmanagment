﻿using HotelManagment.Domain.Models;
using HotelManagment.Domain.Respositories;
using HotelManagment.Persistence.Contexts;
using Microsoft.EntityFrameworkCore;

namespace HotelManagment.Persistence.Repositories
{
    public class HotelRepository : BaseRepository, IHotelRespository
    {
        public HotelRepository(AppDbContext context) : base(context)
        {
        }

        public async Task<IEnumerable<Hotel>> GetAllHotelsForGivenCityAsync(string city)
        {
            return await _context.Hotels.Include(h => h.Rooms).Where(h => h.City == city).OrderByDescending(h=>h.LocalCategory).ToListAsync();
        }


        public List<Hotel> ListAll()
        {
            return  _context.Hotels.Include(h => h.Rooms).ToList();
        }

        public async Task<IEnumerable<Hotel>> ListAsync()
        {
            return await _context.Hotels.Include(h => h.Rooms).ToListAsync();
        }
    }
}
