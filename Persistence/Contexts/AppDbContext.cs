﻿using HotelManagment.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.ValueGeneration;

namespace HotelManagment.Persistence.Contexts
{
    public class OrderIdValueGenerator : ValueGenerator<int>
    {
        private int _current;

        public override bool GeneratesTemporaryValues => false;

        public override int Next(EntityEntry entry)
            => Interlocked.Increment(ref _current);
    }

    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }
        
        public AppDbContext()
        {
        }
        public DbSet<Hotel> Hotels { get; set; }
        public DbSet<Room> Rooms { get; set; }
        protected override void OnConfiguring
      (DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseInMemoryDatabase(databaseName: "DerTouristik");
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            //modelBuilder.Entity<Hotel>().HasKey(c => c.Code);
            //modelBuilder.Entity<Room>().HasKey(r => r.RoomName);
            base.OnModelCreating(builder);
            builder.Entity<Hotel>().ToTable("Hotels");
            builder.Entity<Hotel>().HasKey(h => h.Code);
            //builder.Entity<Hotel>().Property(h => h.Code).IsRequired().ValueGeneratedOnAdd();
            //builder.Entity<Category>().Property(p => p.Name).IsRequired().HasMaxLength(30);
            builder.Entity<Hotel>().HasMany(h => h.Rooms).WithOne(r => r.Hotel).HasForeignKey(r => r.HotelCode);
            builder.Entity<Hotel>().HasData
           (
                  new Hotel
                  {
                      Code = "SPRIN",
                      HotelName = "Springfield Inn",
                      LocalCategory = 3,
                      City = "Springfield",
                      NumberOfRooms = 234,
                  },
                  new Hotel
                    {
                        Code = "SPRPL",
                        HotelName = "Springfield Plaza",
                        LocalCategory = 5,
                        City = "Springfield",
                        NumberOfRooms = 147,
                    },
                  new Hotel
                  {
                      Code = "SPRRE",
                      HotelName = "Springfield Resort",
                      LocalCategory = 4,
                      City = "Springfield",
                      NumberOfRooms = 345,
                  },
                  new Hotel
                   {
                       Code = "SHEMO",
                       HotelName = "Shelbyville Motors",
                       LocalCategory = 3,
                       City = "Shelbyville",
                       NumberOfRooms = 227,
                   },
                  new Hotel
                    {
                        Code = "SHEPL",
                        HotelName = "Shelbyville Plaza",
                        LocalCategory = 4.5,
                        City = "Shelbyville",
                        NumberOfRooms = 84,
                    },
                  new Hotel
                     {
                         Code = "SHERE",
                         HotelName = "Shelbyville Resort",
                         LocalCategory = 3,
                         City = "Shelbyville",
                         NumberOfRooms = 319,
                     }
           );

            builder.Entity<Room>().ToTable("Rooms");
            builder.Entity<Room>().HasKey(p => p.Id);
            //builder.Entity<Room>().Property(p => p.Id).HasValueGenerator<OrderIdValueGenerator>();
            builder.Entity<Room>().Property(p => p.RoomName).IsRequired().HasMaxLength(50);
            builder.Entity<Room>().Property(p => p.PricePerNight).IsRequired();

            builder.Entity<Room>().HasData
                (
                new Room
                {
                    Id = 1,
                    HotelCode= "SPRIN",
                    RoomName= "Double Room Standard",
                    PricePerNight=100.0,
                },
                new Room
                {
                    Id = 2,
                    HotelCode = "SPRIN",
                    RoomName = "Double Room Deluxe",
                    PricePerNight = 150.0,
                },
                new Room
                {
                    Id = 3,
                    HotelCode = "SPRIN",
                    RoomName = "Junior Suite",
                    PricePerNight = 250.0,
                },
                new Room
                {
                    Id = 4,
                    HotelCode = "SPRPL",
                    RoomName = "Double Room Standard",
                    PricePerNight = 120.0,
                },
                new Room
                {
                    Id = 5,
                    HotelCode = "SPRPL",
                    RoomName = "Double Room Deluxe",
                    PricePerNight = 170.0,
                },
                new Room
                {
                    Id = 6,
                    HotelCode = "SPRPL",
                    RoomName = "Junior Suite",
                    PricePerNight = 290.0,
                },
                new Room
                {
                    Id = 7,
                    HotelCode = "SPRPL",
                    RoomName = "Suite",
                    PricePerNight = 350,
                },
                new Room
                {
                    Id = 8,
                    HotelCode = "SPRRE",
                    RoomName = "Single Room Standard",
                    PricePerNight = 80.5,
                },
                new Room
                {
                    Id = 9,
                    HotelCode = "SPRRE",
                    RoomName = "Double Room Standard",
                    PricePerNight = 105.0,
                },
                new Room
                {
                    Id = 10,
                    HotelCode = "SPRRE",
                    RoomName = "Double Room Deluxe",
                    PricePerNight = 165.0
                },
                new Room
                {
                    Id = 11,
                    HotelCode = "SPRRE",
                    RoomName = "Family Room",
                    PricePerNight = 195.0,
                },
                new Room
                {
                    Id = 12,
                    HotelCode = "SHEMO",
                    RoomName = "Double Room Standard",
                    PricePerNight = 90.0,
                },
                new Room
                {
                    Id = 13,
                    HotelCode = "SHEMO",
                    RoomName = "Double Room Deluxe",
                    PricePerNight = 130.0,
                },
                new Room
                {
                    Id = 14,
                    HotelCode = "SHEMO",
                    RoomName = "Junior Suite",
                    PricePerNight = 217.0,
                },
                new Room
                {
                    Id = 15,
                    HotelCode = "SHEPL",
                    RoomName = "Double Room Standard",
                    PricePerNight = 115.0,
                },
                new Room
                {
                    Id = 16,
                    HotelCode = "SHEPL",
                    RoomName = "Double Room Deluxe",
                    PricePerNight = 157.0,
                },
                new Room
                {
                    Id = 17,
                    HotelCode = "SHEPL",
                    RoomName = "Junior Suite",
                    PricePerNight = 256.0,
                },
                new Room
                {
                    Id = 18,
                    HotelCode = "SHEPL",
                    RoomName = "Suite",
                    PricePerNight = 349.0,
                },
                new Room
                {
                    Id = 19,
                    HotelCode = "SHERE",
                    RoomName = "Single Room Standard",
                    PricePerNight = 83.0,
                },
                new Room
                {
                    Id = 20,
                    HotelCode = "SHERE",
                    RoomName = "Double Room Standard",
                    PricePerNight = 107.0,
                },
                new Room
                {
                    Id = 21,
                    HotelCode = "SHERE",
                    RoomName = "Double Room Deluxe",
                    PricePerNight = 149.0,
                },
                new Room
                {
                    Id = 22,
                    HotelCode = "SHERE",
                    RoomName = "Family Room",
                    PricePerNight = 206.0,
                }
                );


        }
       
    }
}
