﻿namespace HotelManagment.Resources
{
    public class RoomResource
    {
        public required string RoomName { get; set; }
        public double PricePerNight { get; set; }
    }
}
