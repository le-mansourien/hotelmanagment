﻿using HotelManagment.Domain.Models;

namespace HotelManagment.Resources
{
    public class HotelResource
    {
        public required string Code { get; set; }
        public int LocalCategory { get; set; }
        public required string City { get; set; }
        public int NumberOfRooms { get; set; }
        public List<RoomResource> Rooms { get; set; } = new List<RoomResource>();
    }
}
